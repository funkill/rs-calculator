<?php

/**
 * Created by PhpStorm.
 * User: funkill
 * Date: 20.07.15
 * Time: 23:40
 */
namespace CRepl;

use Calculator\Exceptions\TypeException;
use Calculator\Interpreter;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

class CCommand extends Command {

    protected function configure() {
        $this
            ->setName("rs-calculator")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        /**
         * @var $qHelper QuestionHelper
         */
        $qHelper = $this->getHelper('question');
        $question = new Question("> ");
        while(true) {
            try {
                $answer = $qHelper->ask($input, $output, $question);
            } catch (\RuntimeException $RE) {
                $output->writeln("");
                $output->writeln("Good bye!");
                return;
            }

            if (!$answer) {
                continue;
            }

            try {
                $result = Interpreter::evaluate($answer);
            } catch (TypeException $TE) {
                $output->writeln("Wrong input!");
                continue;
            } catch (\Exception $E) {
                $output->writeln("Unknown exception");
                continue;
            }

            $output->writeln($result);
        }
    }
}