<?php

/**
 * Created by PhpStorm.
 * User: funkill
 * Date: 20.07.15
 * Time: 23:36
 */

namespace CRepl;

use Symfony\Component\Console\Application;

class CRepl {

    public static function start() {
        $App = new Application();
        $App->add(new CCommand());
        $App->run();
    }

}